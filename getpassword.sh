#!/bin/bash
## USAGE: getpassword.sh provider
## Gets the sftp password stored in the ODB
source ~/.bashrc
source ~/.bash_profile
source ~/.script

PROVIDER="$1"
OUTPUTFILE="$2"
TABLE=sftp
if [ -z "$1" ]
  then
    echo "ERROR" > $OUTPUTFILE
    exit 1
fi
#get password from db
rm dbdump.csv $OUTPUTFILE
db "select address,password from $TABLE where provider='$PROVIDER';" -s dbdump.csv

#remove headerline and remove db characters like () and "
cat dbdump.csv | tr -d '()"' | tail -n +2 > $OUTPUTFILE
