package utils

import (
	"errors"
	"os/exec"
)

func GetSFTPPassword(provider string) (string, string, error) {
	//returns password from odb using a shell script
	var password, address string
	//First check if it's autobid. If so return with address and password immediately
	if provider == "autobid" {
		address = "autobid@inventory.tripadvisor.com"
		password = "wiJO9l5W3i"

	} else {

		tmpFile := "pass.csv"

		exec.Command("/bin/bash", "/home/optimiser/go/src/bitbucket.org/bidop/go-utils/getpassword.sh", provider, tmpFile).Output()
		err := ReadCsv(tmpFile, false, ',', func(l []string) error {
			if len(l) < 2 {
				return errors.New("could not read file")
			}
			address = l[0]
			password = l[1]

			if password == "ERROR" {
				return errors.New("Could not get password")
			}
			return nil
		})
		if err != nil {
			return "", "", err
		}
	}
	return address, password, nil
}
