package utils

import (
    "fmt"
    "os"
    "encoding/csv"
    "errors"
)


func Cut_CSV(fname string, target_columns []string) error {
    // Basic file check
    if len(fname) == 0 {
        return errors.New("no filepaths provided")
    }


    //Read file get header
    f, err := os.Open(fname)
    if err != nil {
        return err
    }
    r := csv.NewReader(f)

    lines, err := r.ReadAll()
    if err != nil{
        return err
    }
    if err = f.Close(); err != nil {
        return err
    }

    //Find columns_index of columns in big file
    target_columns_index := []int{}
    header := lines[0]

    for _, target := range target_columns{
        for j, val := range header {
            if val == target { target_columns_index = append(target_columns_index,j) }

        }

    }

    //write the file
    f, err = os.Create(fname)

    if err != nil{
        return err
    }

    w := csv.NewWriter(f)
    for _, line := range lines {
        new_rows := []string{}
        for _, j := range target_columns_index{
            new_rows = append(new_rows, line[j])
        }

            err = w.Write(new_rows)
            if err != nil {
                fmt.Println(err)
                return err
            }
        }
    w.Flush()
    return f.Close()
}
