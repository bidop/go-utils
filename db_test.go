package utils

import (
	"fmt"
	"testing"
)

func TestCreateTableFromCSV(t *testing.T) {
	// csvfile := "/Users/shahram/Desktop/aus1.csv"
	// err := CreateTableFromCSV("tmp.clicks", Odb, "ds date,country text,traffic_type text,location_id int", csvfile)
	// if err != nil {
	// 	t.Errorf("Failed!" + err.Error())
	// }
}

func TestGetFieldWithCriteria(t *testing.T) {
	// csvfile := "/Users/shahram/Desktop/aus1.csv"
	tablelist, err := GetFieldWithCriteria(false,
		"table_name", "information_schema.tables", "table_schema='tests' and table_type='BASE TABLE' and table_name like 'clicks%'", Odb)
	if err != nil {
		t.Errorf("Failed!" + err.Error())
	}
	t.Log(fmt.Println(tablelist))
}
