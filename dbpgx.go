package utils

//Code to fetch small data from various database switch between databases,

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path"
	"time"

	//package from github for postgres in golang
	"github.com/jackc/pgx"
	"github.com/joho/sqltocsv"
	_ "github.com/lib/pq"
)

const (
	//Rio to connect to Rio
	RioDb = "host='rio-proxy.tripadvisor.com' port='5439' dbname='rio' user='sanver' password='MakersMark1' sslmode=disable"
	//Odb to connect to odb
	OdbDb = "user=optimiser dbname='odb' password='makersmark' sslmode=disable"
)

var (
	Odb = pgx.ConnConfig{Host: "localhost", User: "optimiser", Password: "makersmark", Database: "odb"}
	Rio = pgx.ConnConfig{Host: "rio-proxy.tripadvisor.com", Port: 5439, User: "sanver", Password: "MakersMark1", Database: "rio"}
)

//DBConnect to connect to database of your choice. (Rio or Odb)
func DBConnect(database pgx.ConnConfig) (*pgx.Conn, error) {
	// var conn *pgx.Conn
	// var err error
	conn, err := pgx.Connect(database)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		return nil, err
	}
	return conn, nil
}

//Func to parse string array into '',''
func DateArrayToString(Date []string) (string, error) {
	var out string

	if len(Date) == 0 {
		return "", errors.New("Error processing dates, not enough dates passed")
	}

	for _, j := range Date {
		out += fmt.Sprintf(`'%s',`, j)
	}

	if len(out) < 2 {
		return "", errors.New("Error processing dates, not enough dates passed")
	}
	//remove last comma
	out = out[:len(out)-1]

	return out, nil
}

func CreateTableFromCSV(tableName, columns, csvfile string, database pgx.ConnConfig) error {
	/*
		copy file to /tmp
		create a new table
		pass it to postgres to copy it efficiently into the table
	*/

	//get filename from full path
	_, filename := path.Split(csvfile)
	tmpCsvfile := "/tmp/" + filename

	//copy csv file to /tmp
	cmd := exec.Command("cp", csvfile, tmpCsvfile)
	err := cmd.Run()
	if err != nil {
		return errors.New("Could not copy file:\n" + tableName + " Err: " + err.Error())
	}

	CreateQuery := fmt.Sprintf("create table %s (%s)", tableName, columns)
	err = DoQuery(CreateQuery, database)
	if err != nil {
		return errors.New("Could not create table:\n" + tableName + " Err: " + err.Error())
	}

	CopyQuery := fmt.Sprintf("COPY %s FROM %s CSV HEADER;", tableName, tmpCsvfile)

	err = DoQuery(CopyQuery, database)
	if err != nil {
		return errors.New("Could not copy csv data to table:\n" + tmpCsvfile + " Err: " + err.Error())
	}

	//clear csv file from /tmp
	cmd = exec.Command("rm", tmpCsvfile)
	err = cmd.Run()
	if err != nil {
		return errors.New("Could not remove file from tmp:\n" + tmpCsvfile + " Err: " + err.Error())
	}

	return nil
}

func DropTable(tableName string, database pgx.ConnConfig) error {
	db, err := DBConnect(database)
	if err != nil {
		return err
	}
	defer db.Close()
	query := fmt.Sprintf(`truncate table %s`, tableName)
	DoQuery(query, database)

	query = fmt.Sprintf(`drop table if exists %s`, tableName)
	return DoQuery(query, database)
}

func GetFieldWithCriteria(distinct bool, field, tablename, criteria string, database pgx.ConnConfig) ([]string, error) {
	var output []string
	var query, strDistinct, fielddata string

	db, err := DBConnect(database)
	if err != nil {
		return nil, err
	}
	defer db.Close()

	if distinct == true {
		strDistinct = "distinct "
	}

	if database.Database == Rio.Database {
		DoQuery("set query_group='quick';", Rio)
	}
	if criteria == "" {
		query = "select " + strDistinct + field + " from " + tablename + " order by 1 asc;"
	} else {
		query = "select " + strDistinct + field + " from " + tablename + " where " + criteria + " order by 1 asc;"
	}

	rows, err := db.Query(query)
	if err != nil {
		return nil, err
	}

	if field == "ds" {
		for rows.Next() {
			rows.Scan(&fielddata)
			output = append(output, dateToString(fielddata))
		}
	} else {
		for rows.Next() {
			rows.Scan(&fielddata)
			output = append(output, fielddata)
		}
	}
	return output, nil
}

func GetDistinctField(field, tablename string, database pgx.ConnConfig) ([]string, error) {
	return GetFieldWithCriteria(true, field, tablename, "", database)
}

func dateToString(str string) string {
	//psql output 2016-10-19T00:00:00Z
	layout := "2006-01-02T15:04:05Z"
	t, err := time.Parse(layout, str)
	if err != nil {
		//this is not a date
		return str
	}
	return t.Format("2006-01-02")
}

func DoQuery(query string, database pgx.ConnConfig) error {
	db, err := DBConnect(database)
	if err != nil {
		return err
	}
	defer db.Close()

	// stmt, err := db.Prepare(query)
	// if err != nil {
	// 	msg := "Function DoQuery prep failed, Query:\n" + query
	// 	return errors.New(msg + " Err: " + err.Error())
	// }

	_, err = db.Exec(query)
	if err != nil {
		msg := "Function DoQuery failed, Query:\n" + query
		return errors.New(msg + " Err: " + err.Error())
	}

	return nil
}

//CountriesQueryString, format the query countries from CountryArray
func ArrayToQueryString(dbArray []string) string {
	QueryString := "("
	for _, j := range dbArray {
		QueryString += fmt.Sprintf(`'%s',`, j)
	}

	i := len(QueryString)
	QueryString = QueryString[:i-1]
	QueryString += ")"

	return QueryString
}

//CountriesQueryString, format the query countries from CountryArray
func ArrayToQueryInt(dbArray []int) string {
	QueryString := "("
	for _, j := range dbArray {
		QueryString += fmt.Sprintf(`%d,`, j)
	}

	i := len(QueryString)
	QueryString = QueryString[:i-1]
	QueryString += ")"

	return QueryString
}

//GetProviderID use odb to fetch provider_id of a provider
func GetProviderID(provider string) (int, error) {

	db, err := DBConnect(Odb)
	defer db.Close()

	var Result int

	err = db.QueryRow("Select provider_id from public.providers where provider_name= $1 limit 1", provider).Scan(&Result)

	if err == sql.ErrNoRows {
		return 0, errors.New("GetProviderID: Can't get provider_id for " + provider)
	}
	if err != nil {
		return 0, err
	}

	return Result, nil
}

func DbTableToCsv(database, outFileName, query string, args ...interface{}) error {
	if database == RioDb {
		DoQuery("set query_group='quick';", Rio)
	}

	db, err := sql.Open("postgres", database)
	if err != nil {
		return err
	}
	defer db.Close()

	rows, err := db.Query(query, args...)
	if err != nil {
		println(query)
		return err
	}

	err = sqltocsv.WriteFile(outFileName, rows)
	if err != nil {
		return err
	}
	return nil
}

//DbToCsv is a wrapper function of odb's db command to get csv from odb or Rio
func oldDbTableToCsv(database, query, filePath, outFileName, header string) error {
	// cmd := exec.Command("/bin/bash", config.GetMainDir()+"cluster/db.sh", filePath, outFileName, query, database, header)
	var args = []string{query, "-s", filePath + "/" + outFileName}
	if database == "rio" {
		args = append(args, "-d", "rio")
	}
	if header != "" {
		args = append(args, "-t", "header")
	}
	cmd := exec.Command("db", args...)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		msg := "Could not run db command to extract CSV! " + fmt.Sprint(err) + ": " + stderr.String()
		return errors.New(msg)
	}

	//Lets check if the CSV export worked
	lineCnt, _ := LineCount(outFileName)
	if lineCnt <= 2 {
		msg := "Oops... got an empty CSV file for this query: " + query
		return errors.New(msg)
	}
	return nil
}
