package utils


import (
	"errors"
	"os"

	"os/exec"
	"encoding/csv"
)



func Copy(src, dest string) error {
	_, err := exec.Command("cp", src, dest).Output()
	return err
}


// when we have something like 
// external_id, A, B, C
// and want seperate files called A.csv, B.csv...
// with external_id, A...
// returns filenames, silos, err
func Splitter(filepath, output_folder string) ([]string, []string, error) {
	// output files
	var outputs []*os.File
	var output_writers []*csv.Writer
	var output_filenames []string

	// read the input
	d, _ := GuessDelimiter(filepath)
	count := 0
	var silos []string
	var N int
	err := ReadCsv(filepath, false, d, func(l []string) error {
		count++
		if count == 1 {
			silos = l[1:]
			N = len(silos)
			// open output files for writing
			outputs = make([]*os.File, N)
			output_writers = make([]*csv.Writer, N)
			output_filenames = make([]string, N)
			for i := 0; i < N; i++ {

				filename := output_folder + silos[i] + ".csv"
				output_filenames[i] = filename
				f, err := os.Create(filename)
				if err != nil { return err }
				// defer .Close() not valid this function 
				// is a closure
				writer := csv.NewWriter(f)
				outputs[i] = f
				output_writers[i] = writer


				// write header
				err = output_writers[i].Write([]string{ l[0], l[i+1] })
				if err != nil { return err }
			}

			return nil
		}

		// non header case
		for i := 0; i < N; i++ {
			err := output_writers[i].Write([]string{ l[0], l[i+1] })
			if err != nil { return err }
		}

		return nil
	})

	// flush writers
	for i := 0; i < N; i++ {
		output_writers[i].Flush()
		outputs[i].Close()
	}

	return output_filenames, silos, err


}



// given filpaths to a bunch of csv's, performs a join on 
// pivot_column, where the data_column from each file is appended as 
// a column in the result, with name from names
// filepath[i] -> names[i] order must be maintained
// order or pivot_column values not guaranteed
func Combiner(pivot_column, data_column, output_filepath string, filepaths, names []string) error {
	if len(filepaths) == 0 {
		return errors.New("no filepaths provided")
	}

	if len(filepaths) != len(names) {
		return errors.New("filepaths and names must have same length, with corresponding order")
	}

	pivot_to_entries := make(map[string][]string)


	N := len(filepaths)
	for i, filepath := range filepaths {
		count := 0
		pivot_index := -1
		data_index := -1

		var pivot_value, data_value string
		err := ReadCsv(filepath, false, ',', func(row []string) error {
			if count == 0 {
				count++

				// find pivot column and data column indices
				for j, val := range row {
					if val == pivot_column { pivot_index = j }
					if val == data_column { data_index = j }
				}
				if pivot_index == -1 || data_index == -1 {
					return errors.New("data or pivot column not found in file: " + filepath)
				}

				return nil
			}

			pivot_value = row[pivot_index]
			data_value = row[data_index]

			// if pivot value is in map append the value to appropriate index
			// otherwise add to map
			if _, ok := pivot_to_entries[pivot_value]; ok == false {
				pivot_to_entries[pivot_value] = make([]string, N)
			}
			pivot_to_entries[pivot_value][i] = data_value


			return nil

		})
		if err != nil { return err }
	}


	headers := append([]string{ pivot_column }, names...)
	return writeCombinedCsv(pivot_to_entries, headers, output_filepath)
}



func writeCombinedCsv(m map[string][]string, headers []string, output_filepath string) error {
	csvfile, err := os.Create(output_filepath)
	if err != nil { return err }
	defer csvfile.Close()

	writer := csv.NewWriter(csvfile)

	err = writer.Write(headers)
	if err != nil { return err }

	var record []string
	for k, v := range m {
		record = append([]string{ k }, v...)
		err := writer.Write(record)
		if err != nil { return err }
	}

	writer.Flush()

	return nil
}



func GetColumn(index int, input, output, newheader string) error {
	csvfile, err := os.Create(output)
	if err != nil { return err }
	defer csvfile.Close()
	writer := csv.NewWriter(csvfile)

	d, _ := GuessDelimiter(input)
	count := 0
	err = ReadCsv(input, false, d, func(l []string) error {
		count++
		if len(l) < index { return errors.New("Input not fat enough") }
		if count == 1 {
			header := l[0]
			if newheader != "" { header = newheader }
			return writer.Write([]string{ header, })
		}

		return writer.Write([]string{ l[index], })

	})


	writer.Flush()

	return err
}

