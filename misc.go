package utils

import (
	"errors"
	"math/rand"
	"os"
	"time"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

// generate random string of desired length
var src = rand.NewSource(time.Now().UnixNano())

func RandomString(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

// returns -1 if not found
func FindIndexInSliceString(item string, arr []string) int {
	index := -1
	N := len(arr)
	for i := 0; i < N; i++ {
		if item == arr[i] {
			index = i
			break
		}
	}

	return index
}

func ValidateAttachmentList(attachmentlist []string) ([]string, error) {
	N := len(attachmentlist)
	var err error
	errStr := ""
	for i := 0; i < N; i++ {
		if _, err := os.Stat(attachmentlist[i]); os.IsNotExist(err) {
			//oops file does not exist! this is an Error
			errStr += "ERR Could not find file:" + attachmentlist[i]
			//notifyTechError(pg, err)
			attachmentlist = append(attachmentlist[:i], attachmentlist[i+1:]...) //pop the item out of the array
			N--                                                                  //adjust counter
		}
	}
	if errStr != "" {
		err = nil
	} else {
		err = errors.New(errStr)
	}

	return attachmentlist, err
}
