package utils

import (
	"os"
	"os/exec"
)

func Email(email []string, subject, content string, attachment_path []string) error {
	// Note: content must either be a string or a valid HTML file
	var c1, c2 *exec.Cmd
	var cmdArgs []string

	//first check if we're getting a template html file or just a string of text
	if _, err := os.Stat(content); err == nil { //content is a valid file
		c1 = exec.Command("cat", content)
		cmdArgs = append(cmdArgs, "-e", "set content_type=text/html")
	} else { //content is just a string
		c1 = exec.Command("echo", content)
	}

	//ok now add the standard arguments for mutt
	cmdArgs = append(cmdArgs, "-s", subject)
	cmdArgs = append(cmdArgs, email...)

	if len(attachment_path) != 0 { //add arguments for each attachment provided
		for _, each := range attachment_path {
			cmdArgs = append(cmdArgs, "-a", each)
		}
	}
	c2 = exec.Command("/usr/bin/mutt", cmdArgs...)

	// attachment_path[0] = "-a " + attachment_path[0] //adjust first item in array

	// var attachment_list string
	// if len(attachment_path) > 1 {
	// 	attachment_list = strings.Join(attachment_path, " -a ") //adjust the rest
	// } else {
	// 	attachment_list = attachment_path[0]
	// }

	//  string
	// for _, each := range attachment_path {
	//     	attachment_list = "-a " + each
	//     }
	//c2 = exec.Command("/usr/bin/mutt", "-s", subject, email)
	//"-s", subject, email, attachment_list)

	c2.Stdin, _ = c1.StdoutPipe()
	c2.Stdout = os.Stdout
	var err error
	err = c2.Start()
	if err != nil {
		return err
	}
	err = c1.Run()
	if err != nil {
		return err
	}
	err = c2.Wait()
	if err != nil {
		return err
	}

	return nil
}
