package utils

import

// "time"

(
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

const (
	PRODUCTION_URL = "http://bidop01d.d.tripadvisor.com:5000/api/v1"
	STAGING_URL    = "http://bidop01d.d.tripadvisor.com:5555/api/v1"
	TOKEN          = "61C9FAB71A629397CD23EADED252A"
)

type Time struct {
	time.Time
}

type ReportRequest struct {
	ID        int      `json:"id"`
	Provider  string   `json:"provider"`
	Group     string   `json:"group"`
	Title     string   `json:"title"`
	Fields    []string `json:"fields"`
	Kopititle string   `json:"kopititle"`
	Schedule  string   `json:"schedule"`
	CreatedAt Time     `json:"createdAt"`
	UpdatedAt Time     `json:"updatedAt"`
	UserID    int      `json:"UserId"`
	Kopi      string   `json:"kopi"`
}

func FetchReportRequest(report_title string, staging bool, id string) ([]ReportRequest, error) {
	var result []ReportRequest
	var URL string

	if staging == true {
		URL = STAGING_URL
	} else {
		URL = PRODUCTION_URL
	}

	idFilter := ""
	if id != "" {
		_, err := strconv.Atoi(id)
		if err != nil {
			return nil, errors.New("invalid ID specified: " + id)
		}
		idFilter = "&id=" + id
	}

	url := URL + "/report_requests?title=" + report_title + idFilter + "&token=" + TOKEN
	// println(url)
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Error: " + err.Error())
		return nil, err
	}
	defer resp.Body.Close()

	// parse json
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		fmt.Println("Error: " + err.Error())
		return nil, err
	}

	return result, nil
}

// func filterRequests(requests []ReportRequest) []ReportRequest {
// 	providers := make(map[string]int)
// 	//get top ID for each provider
// 	for _, row := range requests {
// 		_, ok := providers[row.Provider]
// 		if ok == true {
// 			if providers[row.Provider] < row.ID {
// 				providers[row.Provider] = row.ID
// 			}
// 		} else {
// 			providers[row.Provider] = row.ID
// 		}
// 	}

// 	//filter requests based on map
// 	var filteredReqs []ReportRequest
// 	for _, row := range requests {
// 		topID, _ := providers[row.Provider]
// 		if row.ID == topID {
// 			filteredReqs = append(filteredReqs, row)
// 		}
// 	}

// 	return filteredReqs
// }

// func writeRequeststoFile(requests []ReportRequest, filename string) error {
// 	// Lets open a file for writing.
// 	f, err := os.Create(filename)
// 	w := bufio.NewWriter(f)
// 	if err != nil {
// 		println("Couldn't write to " + filename)
// 		return err
// 	}
// 	defer f.Close()

// 	for _, row := range requests {
// 		rowStr := row.Provider + "," + row.Fields[0] + "\n"
// 		_, err := w.WriteString(rowStr)
// 		if err != nil {
// 			println("Couldn't write " + rowStr)
// 			return err
// 		}
// 	}
// 	// Ensure all buffered operations have been applied to the underlying writer.
// 	w.Flush()
// 	return nil
// }
