package utils

import "time"

func ValidateDate(layout, strDate string) error {
	// layout := "2006-01-02T15:04:05.000Z"
	// str := "2014-11-12T11:45:26.371Z"
	_, err := time.Parse(layout, strDate)

	if err != nil {
		return err
	}
	return nil
}
