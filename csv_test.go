package utils

import (
	"testing"
	// "fmt"
)



func TestGuessDelimiter(t *testing.T) {
	path1 := "data/delimiter_test1.csv"
	path2 := "data/test1.csv"
	d1, err := GuessDelimiter(path1)
	if err != nil || d1 != ';' { 
		t.Errorf("Failed to get correct delimiter\n")
	}
	

	d2, err := GuessDelimiter(path2)
	if err != nil || d2 != ',' { 
		t.Errorf("Failed to get correct delimiter\n")
	}
}


func TestSplitter(t *testing.T) {
	path := "data/splitter.csv"

	// Splitter(path, "data/")
	GetColumn(0, path, "moo.csv", "lala int")

}


 // ReadCsv(path string, c chan []string, screenBlank bool, delimiter rune)
func TestReadCsv(t *testing.T) {
	path := "data/csvtest.csv"
	// c := make(chan []string)

	return

	err := ReadCsv(path, true, ',', func(row []string) error {

		return nil
	})

	if err != nil { panic(err) }
	// for record := range c {
	// 	fmt.Println(record)
	// }
}



func TestCombiner(t *testing.T) {
	paths := []string{
		"data/test1.csv",
		"data/test2.csv",
	}
	names := []string{
		"AU-DMeta",
		"CR-Mobile",
	}


	err := Combiner("a", "c", "data/test3.csv", paths, names)
	if err != nil { panic(err) }
}