package utils

import (
	// "fmt"
	"os"
	"os/exec"
	"path/filepath"
	// "strings"
)

func CallPython(args ...string) error {
	_, err := exec.Command("python", args...).Output()
	if err != nil {
		return err
	}
	return nil
}

func CallBash(verbose bool, args ...string) error {
	cmd := exec.Command("bash", args...)
	if verbose {
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
	}
	// cmd.Output()
	err := cmd.Run()
	if err != nil {
		return err
	}

	return nil
}

func Exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

// remove all the files in a dir without
// removing the dir as in os.RemoveAll
func RemoveContents(dir string) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
	return nil
}
