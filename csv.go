package utils

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

const (
	GUESS_DELIMITER_MAX_ROWS = 1000
)

var (
	POSSIBLE_DELIMITERS = []rune{
		',',
		';',
		'\t',
		//      ' ',
	}
)

func GuessDelimiter(filename string) (rune, error) {
	var result rune = ','

	// for each calculate deviation at each iteration
	sum_errors := make(map[rune]int)

	var prev_len, new_len int
	for _, d := range POSSIBLE_DELIMITERS {
		// run through csv
		count := 0
		err := ReadCsv(filename, false, d, func(row []string) error {
			if count > GUESS_DELIMITER_MAX_ROWS {
				return nil
			}
			count++

			if count == 1 {
				prev_len = len(row)
				return nil
			}

			// if the string does not contain the char
			if len(row) == 1 && strings.Contains(row[0], string(d)) == false {
				return nil
			}

			new_len = len(row)
			sum_errors[d] += int(math.Abs(float64(new_len - prev_len)))
			prev_len = new_len

			return nil
		})

		if err != nil {
			return result, err
		}
	}

	// fmt.Printf("%+v\n", sum_errors)

	// take the least error one
	min := MaxIntConst
	for k, v := range sum_errors {
		// println(string(k))
		if v < min {
			result = k
			min = v
		}
	}

	return result, nil
}

// // Validates:
// //  - delimiter is ','. If there is one column and
// //  ';' splits to the same number often, we'll assume ';' is the delimiter
// func ValidateCsv(filename string) error {
//     err := ReadCsv(filename, false, ',', func(row []string) error) error {

//     })
// }

// writes given data to path (absolute recommended), returns
// error if any
func WriteCsv(path string, data [][]string) error {
	f, err := os.Create(path)
	defer f.Close()
	if err != nil {
		return err
	}

	w := csv.NewWriter(f)
	return w.WriteAll(data)
}

func StringsToInts(s []string) ([]int, error) {
	N := len(s)
	var err error
	result := make([]int, N)
	for i := 0; i < N; i++ {
		str := strings.Trim(s[i], " ")
		if str == "" {
			continue
		}
		result[i], err = strconv.Atoi(str)
		if err != nil {
			return nil, err
		}
	}

	return result, nil
}

func isBlank(record []string) bool {
	count := 0
	for _, v := range record {
		if len(strings.Trim(v, " \n\t")) == 0 {
			count++
		}
	}

	if count > 0 {
		return true
	}
	return false
}

/*
	- Reads csv file from path. Returns error if any.
	- Passes records on to provided channel.
	- Will screen blank records if screenBlank set to true. May
	come at slight performance hit.
	rows x cols: O(rows) ==> O(rows x cols)
*/
func ReadCsv(path string, screenBlank bool, delimiter rune, cb func(row []string) error) error {
	// read from csv file
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()
	reader := csv.NewReader(file)

	reader.Comma = delimiter
	reader.LazyQuotes = true
	reader.FieldsPerRecord = -1

	//reader.Read() // skip the header
	for {
		// read just one record, but we could ReadAll() as well
		record, err := reader.Read()
		// end-of-file is fitted into err
		if err == io.EOF {
			break
		} else if err != nil {
			log.Println("Error reading csv file: ", err)
			return err
		}

		// screen for blank record
		// ! this may penalize performance
		if screenBlank == true {
			if isBlank(record) == true {
				continue
			}
		}

		err = cb(record)
		if err != nil {
			return err
		}
		// c <- recorn nil
	}

	// close(c)
	return nil
}

func ReadHeader(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	reader := csv.NewReader(file)
	reader.Comma = ','
	reader.LazyQuotes = true
	reader.FieldsPerRecord = -1

	return reader.Read()
}

// func ReadCsv(path string, c chan []string, screenBlank bool, delimiter rune) error {
//     // read from csv file
//     file, err := os.Open(path)
//     if err != nil { return err }
//     defer file.Close()
//     reader := csv.NewReader(file)

//     reader.Comma = delimiter
//     reader.LazyQuotes = true

//     //reader.Read() // skip the header
//     for {
//         // read just one record, but we could ReadAll() as well
//         record, err := reader.Read()
//         // end-of-file is fitted into err
//         if err == io.EOF {
//             break
//         } else if err != nil {
//             log.Println("Error reading csv file: ", err)
//             return err
//         }

//         // screen for blank record
//         // ! this may penalize performance
//         if screenBlank == true {
//             if isBlank(record) == true { continue }
//         }

//         c <- record
//     }

//     close(c)
//     return nil
// }

// Change_Header
// Usage: Replace header given field[1]: filepath/filename , field[2]: array[headers]
// Example: Change_header("test2.csv", []string{"external_id text", "indexed_volume int"})

func Change_Header(fname string, new_header []string) error {
	f, err := os.Open(fname)
	if err != nil {
		return err
	}
	r := csv.NewReader(f)
	lines, err := r.ReadAll()
	if err != nil {
		return err
	}
	if err = f.Close(); err != nil {
		return err
	}

	//write the file
	f, err = os.Create(fname)

	if err != nil {
		return err
	}
	w := csv.NewWriter(f)
	w.Write(new_header)
	for index, line := range lines {
		if index > 0 {
			err = w.Write(line)
			if err != nil {
				fmt.Println(err)
				return err
			}
		}
	}
	w.Flush()
	return f.Close()
}

//Remove_Header
//Usage: Remove header given field[1]: filepath/filename
//Example: Remove_Header("test2.csv")

func Remove_Header(fname string) error {
	f, err := os.Open(fname)
	if err != nil {
		return err
	}
	r := csv.NewReader(f)
	lines, err := r.ReadAll()
	if err != nil {
		return err
	}
	if err = f.Close(); err != nil {
		return err
	}

	//write the file
	f, err = os.Create(fname)

	if err != nil {
		return err
	}
	w := csv.NewWriter(f)
	for index, line := range lines {
		if index > 0 {
			err = w.Write(line)
			if err != nil {
				fmt.Println(err)
				return err
			}
		}
	}
	w.Flush()
	return f.Close()
}

func LineCount(fname string) (int, error) {
	r, err := os.Open(fname)
	if err != nil {
		return 0, err
	}

	buf := make([]byte, 32*1024)
	count := 0
	lineSep := []byte{'\n'}

	for {
		c, err := r.Read(buf)
		count += bytes.Count(buf[:c], lineSep)

		switch {
		case err == io.EOF:
			return count, nil

		case err != nil:
			return count, err
		}
	}
}
